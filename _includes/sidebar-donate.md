
{% assign donate = strings.sidebars.donate %}
{% assign fallback_donate = fallback_strings.sidebars.donate %}

### {{ donate.heading | default fallback_donate.heading }}

{{ donate.powered_by_donations | default fallback_donate.powered_by_donations }}

{: .donate-options}
 * {:.donate-option .misc} [![{{ donate.paypal_alt | default fallback_donate.paypal_alt }}]({{ site.baseurl }}/assets/paypal_donate_button.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=E2FCXCT6837GL)
 * {:.donate-option .liberapayID} [![{{ donate.liberapay_alt | default fallback_donate.liberapay_alt }}]({{ site.baseurl }}/assets/liberapay_donate_button.svg)](https://liberapay.com/F-Droid-Data/donate)
 * {:.donate-option .flattrID} [![{{ donate.flattr_alt | default fallback_donate.flattr_alt }}]({{ site.baseurl }}/assets/flattr-badge-large.png)](https://flattr.com/thing/343053/F-Droid-Repository)
 * {:.donate-option .bitcoin-blockchain-info} [![{{ donate.bitcoin_alt | default fallback_donate.bitcoin_alt }}]({{ site.baseurl }}/assets/bitcoin.png)](https://blockchain.info/address/15u8aAPK4jJ5N8wpWJ5gutAyyeHtKX5i18)
 * {:.donate-option .bitcoin-url} [`15u8aAPK4jJ5N8wpWJ5gutAyyeHtKX5i18`](bitcoin:15u8aAPK4jJ5N8wpWJ5gutAyyeHtKX5i18)
 * {:.donate-option .bank-transfer} {{ donate.via_bank_transfer | default fallback_donate.via_bank_transfer }}
   * {{ donate.bank_transfer_name | default fallback_donate.bank_transfer_name }}: F-Droid Limited
   * IBAN: GB92 BARC 2056 7893 4088 92
   * SWIFT: BARCGB22
 * {:.donate-option .hellotux} {{ donate.hellotux | default fallback_donate.hellotux }}
   * [![{{ donate.hellotux_alt | default fallback_donate.hellotux_alt }}]({{ site.baseurl }}/assets/hellotux_banner.jpg)](https://www.hellotux.com/f-droid)
   * {{ donate.hellotux_donation | default fallback_donate.hellotux_donation }}
